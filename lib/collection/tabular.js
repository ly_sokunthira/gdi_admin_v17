import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Ksp } from './createdocdb.js';
import { Historys } from './runningdocdb.js';
import { Typedocs } from '../collection/typedocdb.js';
import { Docout } from '../collection/documentoutdb.js';

export const TabularTables = {};

TabularTables.Typedocs = new Tabular.Table({
    name: "Typedocs",
    collection: Typedocs,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[0, "DESC"]],
    columns: [
        {data: 'txtcode', title: 'លេខកូដ' },
        {data: 'txttype', title: 'ប្រភេទឯកសារ' },
        {
            data: "_id", width:'250px', orderable: false, className: "text-center",
            title: "កែសម្រួល",
            render: function (e){
                var btntxt = new Spacebars.SafeString("<a href='' id='edit_typedoc' class='btn btn-outline-primary'><i class='fa fa-edit'></i> កែសម្រួល</a> ");
                return btntxt + new Spacebars.SafeString("<a href='' id='delete_typedoc' class='btn btn-outline-danger'><i class='fa fa-trash'></i> លុបចោល</a>");
            }
        }
    ]
});

TabularTables.Ksp = new Tabular.Table({
    name: "Ksp",
    collection: Ksp,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[1, "DESC"]],
    columns: [
        {data: 'txtcodedoc', title: 'លេខកូដ' },
        {data: 'txtdatein', title: 'កាលបរិច្ឆេទ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtdatedoc', title: 'កាលបរិច្ឆេទឯកសារ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtcreate', title: 'ស្ថាប័នរៀបចំលិខិត' },
        {data: 'txtdescription', title: 'បរិយាយ' },
        {
            data: "_id", width:'250px', orderable: false, className: "text-center",
            title: "កែសម្រួល",
            render: function (e){
                var btntxt = new Spacebars.SafeString("<a href='' id='edit_createdoc' class='btn btn-outline-primary'><i class='fa fa-edit'></i> កែសម្រួល</a> ");
                return btntxt + new Spacebars.SafeString("<a href='' id='delete_createdoc' class='btn btn-outline-danger'><i class='fa fa-trash'></i> លុបចោល</a>");
            }
        }
    ]
});

TabularTables.Historys = new Tabular.Table({
    name: "Historys",
    collection: Historys,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[1, "desc"]],
    columns: [
        {data: 'txtcodedoc', title: 'លេខកូដ' },
        {data: 'txtdatein', title: 'កាលបរិច្ឆេទ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtdatedoc', title: 'កាលបរិច្ឆេទឯកសារ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtcreate', title: 'ស្ថាប័នរៀបចំលិខិត' },
        {data: 'txtdescription', title: 'បរិយាយ' },
        {data: 'txtsent', title: 'អ្នកបញ្ជូន' },
        {data: 'txtuser', title: 'សិ្ថតនៅ' },
        {data: 'txtdatesend', title: 'កាលបរិច្ឆេទទទួល',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtnote', title: 'ចំណារថ្នាក់ដឹកនាំ' },
        // {
        //     data: "_id", width:'250px', orderable: false, className: "text-center",
        //     title: "កែសម្រួល",
        //     // render: function (e){
        //     //     var btntxt = new Spacebars.SafeString("<a href='' id='edit_runningdoc' class='btn btn-outline-primary'><i class='fa fa-edit'></i> កែសម្រួល</a> ");
        //     //     return btntxt + new Spacebars.SafeString("<a href='' id='delete_runningdoc' class='btn btn-outline-danger'><i class='fa fa-trash'></i> លុបចោល</a>");
        //     // }
        // }
    ]
});

TabularTables.Docout = new Tabular.Table({
    name: "Docout",
    collection: Docout,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[1, "DESC"]],
    columns: [
        {data: 'txtdocoutcode', title: 'លេខកូដ' },
        {data: 'txtdocoutdate', title: 'កាលបរិច្ឆេទឯកសារ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtdescription', title: 'ខ្លឹមសារឯកសារ'},
        {data: 'txtreceive', title: 'បញ្ជូនទៅ' },
        {data: 'txtreceivedate', title: 'កាលបរិច្ឆេទទទួល',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtreceiver', title: 'អ្នកទទទួល' },
        {
            data: "_id", width:'250px', orderable: false, className: "text-center",
            title: "កែសម្រួល",
            render: function (e){
                var btntxt = new Spacebars.SafeString("<a href='' id='edit_docout' class='btn btn-outline-primary'><i class='fa fa-edit'></i> កែសម្រួល</a> ");
                return btntxt ;
            }
        }
    ]
});

TabularTables.Docout1 = new Tabular.Table({
    name: "Docout1",
    collection: Docout,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[1, "DESC"]],
    columns: [
        {data: 'txtdocoutcode', title: 'លេខកូដ' },
        {data: 'txtdocoutdate', title: 'កាលបរិច្ឆេទឯកសារ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtdescription', title: 'ខ្លឹមសារឯកសារ'},
        {data: 'txtreceive', title: 'បញ្ជូនទៅ' },
        {data: 'txtreceivedate', title: 'កាលបរិច្ឆេទទទួល',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtreceiver', title: 'អ្នកទទទួល' }
    ]
});