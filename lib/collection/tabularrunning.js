import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Ksp } from './createdocdb.js';
export const TabularTables = {};
TabularTables.Ksp = new Tabular.Table({
    name: "Ksp",
    collection: Ksp,
    responsive: true,
    bFilter: true,
    stateSave: true,
    autoWidth: false,
    order: [[1, 'desc']],
    // selector() {
    //     return {txtuser: Meteor.user().profile.position};
    // },
    columns: [
        {data: 'txtcodedoc', title: 'លេខកូដ' },
        {data: 'txtdatein', title: 'កាលបរិច្ឆេទ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtdatedoc', title: 'កាលបរិច្ឆេទឯកសារ',
            render: function (val, type, doc) {
                if (val instanceof Date) {
                return moment(val).format("DD-MM-YYYY");
                } else {
                return "Never";
                }
            }
        },
        {data: 'txtcreate', title: 'ស្ថាប័នរៀបចំលិខិត' },
        {data: 'txtdescription', title: 'បរិយាយ' },
        {
            data: "_id", width:'250px', orderable: false, className: "text-center",
            title: "កែសម្រួល",
            render: function (e){
                var btntxt = new Spacebars.SafeString("<a href='' id='send_createdoc' class='btn btn-outline-primary'><i class='fa fa-edit'></i> បញ្ជូនឯកសារ</a> ");
                return btntxt;
            }
        }
    ]
});