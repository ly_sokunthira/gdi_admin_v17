import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
export const Ksp = new Mongo.Collection('ksp');
const Schemas = {};
Schemas.Ksp = new SimpleSchema({
    txtcodedoc: {
        type: String,
        label: "txtcodedoc",
        max: 200
    },
    txtdatein: {
        type: Date,
        label: "txtdatein"
    },
    txtdatedoc: {
        type: Date,
        label: "txtdatedoc"
    },
    txtcreate: {
        type: String,
        label: "txtcreate",
        max: 200
    },
    txtdescription: {
        type: String,
        label: "txtdescription"
    },
    txtuser: {
        type: String,
        label: "txtuser",
        max: 200
    },
    txtstate: {
        type: String,
        label: "txtstate",
        max: 200
    },
    txttimein: {
        type: Date,
        label: "txttimein"
    }
});
Ksp.attachSchema(Schemas.Ksp);