import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
export const Docout = new Mongo.Collection('docout');
const Schemas = {};
Schemas.Docout = new SimpleSchema({
    txtdocoutcode: {
        type: String,
        label: "txtdocoutcode",
        max: 200
    },
    txtdocoutdate: {
        type: Date,
        label: "txtdocoutdate"
    },
    txtdescription: {
        type: String,
        label: "txtdescription"
    },
    txtreceive: {
        type: String,
        label: "txtreceive",
        max: 200
    },
    txtreceivedate: {
        type: Date,
        label: "txtreceivedate",
    },
    txtreceiver: {
        type: String,
        required: false,
        label: "txtreceiver",
        max: 200
    },
    txttime: {
        type: Date,
        label: "txttime"
    }
});
Docout.attachSchema(Schemas.Docout);