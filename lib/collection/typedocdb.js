import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
export const Typedocs = new Mongo.Collection('typedocs');
const Schemas = {};
Schemas.Typedocs = new SimpleSchema({
    txtcode: {
        type: String,
        label: "txtcode",
        max: 200
    },
    txttype: {
        type: String,
        label: "txttype",
        max: 200
    }
});
Typedocs.attachSchema(Schemas.Typedocs);