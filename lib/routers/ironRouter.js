import '../../client/AppLayout.html';
Router.configure({ 
  layoutTemplate : 'appLayout' 
}); 
Router.route('/appLayout', function () {
  if(Meteor.user()){
    this.render('appLayout');
  } 
});  
Router.route('/', function () { 
    this.render('login'); 
  });  
Router.route('/index', function () { 
  if(Meteor.user()){
    this.render('index'); 
  }
});
Router.route('/signup', function () {
  this.render('signup'); 
});
Router.route('/createdoc', function () { 
  if(Meteor.user()){
  this.render('createdoc');
  }
});
Router.route('/runningdoc', function () { 
  if(Meteor.user()){
    this.render('runningdoc');
  }
});
Router.route('/searchdoc', function () {
    if(Meteor.user()){
        this.render('searchdoc');
    }
});
Router.route('/typedoctemp', function () { 
    if(Meteor.user()){
        this.render('typedoctemp');
    }
});
Router.route('/documentout', function () { 
  if(Meteor.user()){
  this.render('documentout');
  }
});