import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {Meteor} from 'meteor/meteor';

import './main.html';
import './html/createdoc.html';
import './html/login.html';
import './html/signup.html';
import './html/tapbar.html';
import './html/menu.html';
import './html/login.html';
import './html/runningdoc.html';
import './html/searchdoc.html';
import './html/typedoc.html';
import './html/index.html';
import './html/docout.html';

import '../lib/routers/ironRouter.js';
import '../lib/collection/tabular.js';
import '../lib/collection/tabularrunning.js';
import './js/signup.js';
import './js/login.js';
import './js/createdoc.js';
import './js/runningdoc.js';
import './js/searchdoc.js';
import './js/typedoc.js';
import './js/menu.js';
import './js/index.js';
import './js/docout.js';