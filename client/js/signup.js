Template.signup.events({
    'click #signupdata': function(e){
		var pos = trimInput(document.getElementById("idposition").value);
		var tpermis = trimInput(document.getElementById("idpermis").value);
		var toffice = trimInput(document.getElementById("idoffice").value);
		var name = trimInput(document.getElementById("tFirstName").value);
      	var email = trimInput(document.getElementById("tEmail").value);
		var password = trimInput(document.getElementById("tpassword").value);
		//   var password2 = trimInput(document.getElementById("pass2").value);
		// alert(pos);
		Accounts.createUser({
			profile: {position:pos, permis:tpermis, office:toffice},
			username: name,
			email: email,
			password: password,
			//Roles.addUsersToRoles(Meteor.userId, 'admin', null)
		});
		//Roles.addUsersToRoles(Meteor.userId, 'admin', null);
		Router.go("/index");
		return false;
    }
});

// // Validation Rules

// // Trim Helper
var trimInput = function(val){
	return val.replace(/^\s*|\s*$/g, "");
};

// var isNotEmpty = function(value){
// 	if (value && value !== ''){
// 		return true;
// 	}
// 	Bert.alert("Please fill in all fields", "danger", "growl-top-right");
// 	return false;
// };

// // Validate Email
// isEmail = function(value) {
// 	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
// 	if(filter.test(value)) {
// 		return true;
// 	}
// 	Bert.alert("Please use a valid email address", "danger", "growl-top-right");
// 	return false;
// };

// // Check Password Field
// isValidPassword = function(password){
// 	if(password.length <6) {
// 		Bert.alert("Password must be at least 6 characters", "danger", "growl-top-right");
// 		return false;
// 	}
// 	return true;
// };

// // Match Password
// areValidPasswords = function(password, confirm) {
// 	if(!isValidPassword(password)) {
// 		return false;
// 	}
// 	if(password !== confirm) {
// 		Bert.alert("Passwords do not match", "danger", "growl-top-right");
// 		return false;
// 	}
// 	return true;
// };


// Template.hello.onCreated(function helloOnCreated() {
//   // counter starts at 0
//   this.counter = new ReactiveVar(0);
// });

// Template.hello.helpers({
//   counter() {
//     return Template.instance().counter.get();
//   },
// });

// Template.hello.events({
//   'click button'(event, instance) {
//     // increment the counter when button is clicked
//     instance.counter.set(instance.counter.get() + 1);
//   },
// });
