import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Ksp } from '../../lib/collection/createdocdb.js';
import '../html/index.html';
Meteor.subscribe('typedocs');
Meteor.subscribe('ksp');

// Template.index.helpers({
//     ksp(){
//         var dtdoc = document.getElementById("tsearchdate").getAttribute('value');
//         alert(dtdoc);
//         return Ksp.find({txtdatein: formatMontlyDate(dtdoc)}).count();
//     },
// });


Template.registerHelper('daily',function(){
    var dt = new Date();
    return formatDate(dt);
});

// Daily Report
Template.registerHelper('dailydata',function(){
    var dt = new Date();
    var DTA =  "អ្នករៀបចំកំណត់បង្ហាញ";
    return Ksp.find({$and:[{txtuser: DTA},{txtdatein: formatDate(dt)}]}).count();
    // return Ksp.find({txtdatein});
});

Template.registerHelper('dailyBoss',function(){
    var dt = new Date();
    var doc =  "អគ្គនាយករងទទួលជំនាញ";
    return Ksp.find({ $and:[{txtuser: doc},{txtdatein: formatDate(dt)}]}).count();
});
// Template.registerHelper('dailyBigBoss',function(){
//     var dt = new Date();
//     var doc =  "អគ្គនាយក";
//     return Ksp.find({ $and:[{txtuser: doc},{txtdatein: formatDate(dt)}]}).count();
// });
Template.registerHelper('dailydepartment',function(){
    var dt = new Date();
    var doc =  "នាយកដ្ឋានរដ្ឋបាល";
    return Ksp.find({ $and:[{txtuser: doc},{txtdatein: formatDate(dt)}]}).count();
});
Template.registerHelper('dailyDone',function(){
    var dt = new Date();
    var doc =  "បញ្ជូនចេញរួចរាល់";
    return Ksp.find({ $and:[{txtuser: doc},{txtdatein: formatDate(dt)}]}).count();
});



function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-');
}

// Monthly Report
Template.registerHelper('MonthlyAdmin',function(){
    // var dt = new Date();
    var MA =  "A1";
    //return Ksp.find({ $and:[{txtcodedoc: doc},{txtdatein: formatDate(dt)}]}).count();
    return Ksp.find({ txtcodedoc: MA}).count();
});


// Document Stay on
Template.registerHelper('DocstayAdmin',function(){
    // var dt = new Date();
    var DSA =  "អ្នករៀបចំកំណត់បង្ហាញ";
   return Ksp.find({ txtuser: DSA }).count();
});
Template.registerHelper('DocstayBoss',function(){
    // var dt = new Date();
    var DSB =  "អគ្គនាយករងទទួលជំនាញ";
   return Ksp.find({txtuser: DSB}).count();
});
Template.registerHelper('DocstayBigBoss',function(){
    // var dt = new Date();
    var DSBB =  "អគ្គនាយក";
   return Ksp.find({txtuser: DSBB}).count();
});
Template.registerHelper('DocstayDepartment',function(){
    // var dt = new Date();
    var DSD =  "នាយកដ្ឋានរដ្ឋបាល";
   return Ksp.find({txtuser: DSD}).count();
});
Template.registerHelper('Docdone',function(){
    // var dt = new Date();
    var DD =  "បញ្ជូនចេញរួចរាល់";
   return Ksp.find({txtuser: DD}).count();
});