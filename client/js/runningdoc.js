import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Historys } from '../../lib/collection/runningdocdb.js';
import { Ksp} from '../../lib/collection/createdocdb.js';
import '../html/runningdoc.html';
import { TabularTables } from '../../lib/collection/tabularrunning.js';

Meteor.subscribe('historys');
Meteor.subscribe('ksp');

Template.runningdoc.events({
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();
        var txtid= rowData._id;
        var codedoc = rowData.txtcodedoc;
        var datein = rowData.txtdatein;
        var datedoc = rowData.txtdatedoc;
        var create = rowData.txtcreate;
        var description = rowData.txtdescription;
        var tsent = Meteor.user().profile.position;
        var user = document.getElementById("iruning").value;
        var datesend = document.getElementById("idatesend").value;
        var receive = document.getElementById("ireceive").value;
        var note = document.getElementById("inote").value;
        var tstate = '1';
        console.log(Session.get('tabular-filter'));
        if(event.target.id == "send_createdoc"){
            if (user == '' || datesend=='' || receive==''){
                alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
            }
            else {
                if(confirm('តើលោកអ្នកពិតជាចង់បញ្ជូនឯកសារនេះមែនឬទេ?')){
                    Session.set("assetid", rowData._id);
                    Meteor.call("run_sent",txtid,codedoc,datein,datedoc,create,description,tsent,user,datesend,receive,note,tstate);
                    alert("ការបញ្ជូនទទួលបានជោគជ័យ");
                    document.getElementById("iruning").value='';
                    document.getElementById("idatesend").value='';
                    document.getElementById("ireceive").value='';
                }
            }
        }

    }
});
  
Template.runningdoc.helpers({
    // Ksp: function (event){
    //     return TabularTables.Ksp;
    // }
    selector: function () {
        return {
          txtuser: Meteor.user().profile.position,
        };
      }
});