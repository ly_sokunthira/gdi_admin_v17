import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Docout } from '../../lib/collection/documentoutdb.js';
import '../html/docout.html';
import { TabularTables } from '../../lib/collection/tabular.js';
import { Confirmation } from 'meteor/matdutour:popup-confirm';

Meteor.subscribe('docout');

Template.documentout.events({
    'click #sendingdoc' (e) {
        e.preventDefault();
        var tdocoutdoc = document.getElementById("idocoutcode").value;
        var tdocoutdate = formatDate(document.getElementById("idocoutdate").value);
        var tdescription = document.getElementById("idescription").value;
        var treceive = document.getElementById("ireceive").value;
        var treceivedate = formatDate(document.getElementById("ireceivedate").value);
        var treceiver = document.getElementById("ireceiver").value;
        if (tdocoutdoc == '' || tdocoutdate=='' || treceive==''){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ.....");
        }
        else if(tdocoutdoc == this.txtdocoutdoc){
            alert("សូមពិនិត្យម្តងទៀត ទិន្នន័យមានរួចហើយ!!!!");
        }
        else{
            Meteor.call("notedocout",tdocoutdoc,tdocoutdate,tdescription,treceive,treceivedate,treceiver);
            // Clear form
            document.getElementById("idocoutcode").value = '';
            document.getElementById("idocoutdate").value = '';
            document.getElementById("idescription").value = '';
            document.getElementById("ireceive").value= '';
            document.getElementById("ireceivedate").value = '';
            document.getElementById("ireceiver").value = '';
            alert("ទិន្នន័យបញ្ចូលរួចរាល់ហើយ!!!...");
        }
    },
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();
        
        var userPermission = document.getElementById("permis1").getAttribute('value');
        var tdocoutdoc = document.getElementById("idocoutcode").value;
        var tdocoutdate = formatDate(document.getElementById("idocoutdate").value);
        var tdescription = document.getElementById("idescription").value;
        var treceive = document.getElementById("ireceive").value;
        var treceivedate = formatDate(document.getElementById("ireceivedate").value);
        var treceiver = document.getElementById("ireceiver").value;
        if(event.target.id == "edit_docout"){
            if (tdocoutdoc == '' || tdocoutdate=='' || tdescription=='' || treceive==''||  treceivedate==''){
                alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ និងកាលបរិច្ឆេទ!!!...");
            }
            else {
                if(userPermission != 'Admin' && userPermission != 'Manage'){
                    alert("អ្នកជា "+ userPermission +" គ្មានសិទ្ធក្នុងការដែលសម្រួល"); 
                    return;
                }
                else{
                    Session.set("assetid", rowData._id);
                    Meteor.call("docout_edit",rowData._id,tdocoutdoc,tdocoutdate,tdescription,treceive,treceivedate,treceiver);
                    document.getElementById("idocoutcode").value = '';
                    document.getElementById("idocoutdate").value = '';
                    document.getElementById("idescription").value = '';
                    document.getElementById("ireceive").value= '';
                    document.getElementById("ireceivedate").value = '';
                    document.getElementById("ireceiver").value = '';
                    alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
                }
            }
        }
        // if(event.target.id == "delete_createdoc"){
        //     new Confirmation({
        //         message: "តើអ្នកចង់លុបទិន្នន័យមែនឬទេ?",
        //         title: "Confirmation",
        //         cancelText: "No",
        //         okText: "Yes",
        //         success: true, // whether the button should be green or red
        //         focus: "cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
        //     }, function (ok) {
        //         if(ok) Meteor.call('doc_remove', rowData._id, function(err, rs){
        //             if(!rs){
        //                 alert("ទិន្នន័យត្រូវបានលុបចេញ!!!!");
        //             }
        //         });
        //     });
        // }
    }
});
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-');
}
Template.documentout.helpers({
    Docout: function (event){
        return TabularTables.Docout;
    }
});