import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Ksp } from '../../lib/collection/createdocdb.js';
import { Typedocs } from '../../lib/collection/typedocdb.js';
import '../html/createdoc.html';
import { TabularTables } from '../../lib/collection/tabular.js';
import { Confirmation } from 'meteor/matdutour:popup-confirm';

Meteor.subscribe('typedocs');
Meteor.subscribe('ksp');

Template.createdoc.events({
    'click #inputdoc' (e) {
        e.preventDefault();
        var txtcodedoc = document.getElementById("icodedoc").value;
        var txttypedoc = document.getElementById("itypedoc").value;
        var codedoc = txtcodedoc+txttypedoc;
        var datein = formatDate(document.getElementById("idatedocin").value);
        var datedoc = formatDate(document.getElementById("idatedoc").value);
        var create = document.getElementById("icreatedoc").value;
        var description = document.getElementById("idescription").value;
        var user = Meteor.user().profile.position;
        var state = "0";
        if (txtcodedoc == '' || txttypedoc=='' || datein==''){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ.....");
        }
        else if(txtcodedoc == this.txtcodedoc){
            alert("សូមពិនិត្យម្តងទៀត ទិន្នន័យមានរួចហើយ!!!!");
        }
        else{
            // alert(datein);
            Meteor.call("createdoc",codedoc,datein,datedoc,create,description,user,state);
            // Clear form
            document.getElementById("icodedoc").value = '';
            document.getElementById("itypedoc").value = '';
            document.getElementById("idatedocin").value = '';
            document.getElementById("idatedoc").value= '';
            document.getElementById("icreatedoc").value = '';
            document.getElementById("idescription").value = '';
            alert("ទិន្នន័យបញ្ចូលរួចរាល់ហើយ!!!...");
        }
    },
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();
        
        var userPermission = document.getElementById("permis1").getAttribute('value');
        var Ucodedoc = document.getElementById("icodedoc").value;
        var Utypedoc = document.getElementById("itypedoc").value;
        var codedoc = Ucodedoc+Utypedoc;
        var datein = formatDate(document.getElementById("idatedocin").value);
        var datedoc = formatDate(document.getElementById("idatedoc").value);
        var create = document.getElementById("icreatedoc").value;
        var description = document.getElementById("idescription").value;
        var user = Meteor.user().profile.position;
        var state = "0";
        if(event.target.id == "edit_createdoc"){
            if (codedoc == '' || Utypedoc=='' || datein=='' || datedoc==''||  create==''|| description==''){
                alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ និងកាលបរិច្ឆេទ!!!...");
            }
            else {
                if(userPermission != 'Admin' && userPermission != 'Manage'){
                    alert("អ្នកជា "+ userPermission +" គ្មានសិទ្ធក្នុងការដែលសម្រួល"); 
                    return;
                }
                else{
                    Session.set("assetid", rowData._id);
                    Meteor.call("doc_edit",rowData._id,codedoc,datein,datedoc,create,description,user,state);
                    document.getElementById("icodedoc").value='';
                    document.getElementById("itypedoc").value='';
                    document.getElementById("idatein").value='';
                    document.getElementById("idatedoc").value='';
                    document.getElementById("icreate").value='';
                    document.getElementById("idescription").value='';
                    alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
                }
            }
        }
        if(event.target.id == "delete_createdoc"){
            new Confirmation({
                message: "តើអ្នកចង់លុបទិន្នន័យមែនឬទេ?",
                title: "Confirmation",
                cancelText: "No",
                okText: "Yes",
                success: true, // whether the button should be green or red
                focus: "cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
            }, function (ok) {
                if(ok) Meteor.call('doc_remove', rowData._id, function(err, rs){
                    if(!rs){
                        alert("ទិន្នន័យត្រូវបានលុបចេញ!!!!");
                    }
                });
            });
        }
    }
});
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-');
}
Template.createdoc.helpers({
    typedocs(){
        return Typedocs.find({});
    },
    Ksp: function (event){
        return TabularTables.Ksp;
    }
});