import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Typedocs } from '../../lib/collection/typedocdb.js';
import '../html/typedoc.html';
import { TabularTables } from '../../lib/collection/tabular.js';
import { Confirmation } from 'meteor/matdutour:popup-confirm';

Meteor.subscribe('typedocs');

Template.typedoctemp.helpers({
    Typedocs: function (event){
        return TabularTables.Typedocs;
    }
});
Template.typedoctemp.events({
    'click #inputtype'(e) {
        e.preventDefault();
        var code = document.getElementById("tcode").value;
        var type = document.getElementById("ttype").value;
        // alert(txtcode);
        if (code == '' || type=='' ){
            alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
        }
        else {
            Meteor.call("createtypedocs",code,type);  
            // Clear form
            document.getElementById("tcode").value = '';
            document.getElementById("ttype").value = '';
            alert("ទិន្នន័យបញ្ចូលរួចរាល់ហើយ!!!..."); 
        }
    },
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();
        var Ucode = document.getElementById("tcode").value;
        var Utype = document.getElementById("ttype").value;
        if(event.target.id == "edit_typedoc"){
            if (Ucode == '' || Utype=='' ){
                alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
            }
            else {
                Session.set("assetid", rowData._id);
                Meteor.call("edittypedocs",rowData._id,Ucode,Utype)
                document.getElementById("tcode").value='';
                document.getElementById("ttype").value='';
                alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
            }
        }
        if(event.target.id == "delete_typedoc"){
            new Confirmation({
                message: "តើអ្នកចង់លុបទិន្នន័យមែនឬទេ?",
                title: "Confirmation",
                cancelText: "No",
                okText: "Yes",
                success: true, // whether the button should be green or red
                focus: "cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
            }, function (ok) {
                if(ok) Meteor.call('removetypedocs', rowData._id, function(err, rs){
                    if(!rs){
                        alert("ទិន្នន័យត្រូវបានលុបចេញ!!!!");
                    }
                });
            });
        }
    }
});

// Template.typedoc.events({
//     'click #editme'(e){
//         e.preventDefault();
//         var postId = e.currentTarget.dataset.postId;
//         var Ucode = document.getElementById("tcode").value;
//         var Utype = document.getElementById("ttype").value;
//         if (Ucode == '' || Utype=='' ){
//             alert("សូមបញ្ចូលទិន្នន័យសំខាន់ៗ!!!...");
//         }
//         else {
//             Meteor.call("edittypedocs",postId,Ucode,Utype)
//             document.getElementById("tcode").value='';
//             document.getElementById("ttype").value='';
//             alert("ទិន្នន័យកែសម្រួលរួចរាល់ហើយ!!!...");
//         }
//     }
// });
