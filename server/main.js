import { Meteor } from 'meteor/meteor';
import { Typedocs } from '../lib/collection/typedocdb.js';
import { Ksp } from '../lib/collection/createdocdb.js';
import { Historys }  from '../lib/collection/runningdocdb.js';
import { Docout }  from '../lib/collection/documentoutdb.js';
import '../lib/collection/tabular.js';


Meteor.publish('typedocs', function () {
  return Typedocs.find({});
});
Meteor.publish('ksp', function () {
  return Ksp.find({});
});
Meteor.publish('historys', function () {
    return Historys.find({});
});
Meteor.publish('docout', function () {
    return Docout.find({});
});
//Typedocs Code
Meteor.methods({
    'createtypedocs': function (code,type) {
        Typedocs.insert({
            txtcode: code,
            txttype: type
        }); 
    },
    'edittypedocs': function(typeID,Ucode,Utype){
        Typedocs.update(typeID,{$set:{txtcode:Ucode,
            txttype:Utype}
        });
    },
    'removetypedocs': function(typeID){
        // check(docID,String);
        Typedocs.remove(typeID);
    }
});

//Create Document Code
Meteor.methods({
  'createdoc': function (codedoc,datein,datedoc,create,description,user,state) {
      Ksp.insert({
          txtcodedoc: codedoc,
          txtdatein: datein,
          txtdatedoc: datedoc,
          txtcreate: create,
          txtdescription: description,
          txtuser: user,
          txtstate: state,
          txttimein: new Date().toLocaleString()
      });
  },
  'doc_remove': function(docID){
      // check(docID,String);
      Ksp.remove(docID);
  },
  'doc_edit': function(docID,codedoc,datein,datedoc,create,description,user,state){
      Ksp.update(docID,{$set:{
          txtcodedoc: codedoc,
          txtdatein: datein,
          txtdatedoc: datedoc,
          txtcreate: create,
          txtdescription: description,
          txtuser: user,
          txtstate: state,
          txttimein: new Date().toLocaleString()}
      });
  }
});

//Running Document Code
Meteor.methods({
    'run_sent': function (docID,codedoc,datein,datedoc,create,description,sent,user,datesend,receive,note,state){
        Historys.insert({
            txtid: docID,
            txtcodedoc: codedoc,
            txtdatein: datein,
            txtdatedoc: datedoc,
            txtcreate: create,
            txtdescription: description,
            txtsent: sent,
            txtuser: user,
            txtdatesend: datesend,
            txtreceive: receive,
            txtnote: note,
            txtstate: state,
            txttimein: new Date().toLocaleString()
        }); 
        Ksp.update(docID,{$set:{
            txtuser: user,
            txtstate: state
        }});
    }
});

//Document Out
Meteor.methods({
    'notedocout': function (docoutcode,docoutdate,description,receive,receivedate,receiver) {
        Docout.insert({
            txtdocoutcode: docoutcode,
            txtdocoutdate: docoutdate,
            txtdescription: description,
            txtreceive: receive,
            txtreceivedate: receivedate,
            txtreceiver: receiver,
            txttime: new Date().toLocaleString()
        });
    },
    'docout_remove': function(docoutID){
        // check(docID,String);
        Docout.remove(docoutID);
    },
    'docout_edit': function(docoutID,docoutcode,docoutdate,description,receive,receivedate,receiver){
        Docout.update(docoutID,{$set:{
            txtdocoutcode: docoutcode,
            txtdocoutdate: docoutdate,
            txtdescription: description,
            txtreceive: receive,
            txtreceivedate: receivedate,
            txtreceiver: receiver,
            txttimein: new Date().toLocaleString()}
        });
    }
  });

Meteor.startup(() => {
  // code to run on server at startup
});
